package com.example.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "fileUploadFeignClient",url = "http://localhost:8081")
public interface FileUploadFeignClient {

	@PostMapping(value="/upload-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	ResponseEntity<Object> uploadFile(@RequestPart(name = "file") MultiValueMap<String, Object> file);

	@PostMapping(value="/upload-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	ResponseEntity<Object> uploadFile2(@RequestPart(name = "file") MultipartFile file);

	@PostMapping(value="/upload-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	ResponseEntity<Object> uploadFile1(@PathVariable(name = "file") MultipartFile file);
	
	
}
