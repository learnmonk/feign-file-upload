package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileUploadController {

	@Autowired
	FileUploadService fileUploadService;
	
	@PostMapping("/upload-file")
	public ResponseEntity uploadFile() {
		fileUploadService.uploadFile();
		return ResponseEntity.noContent().build();
	}
}
